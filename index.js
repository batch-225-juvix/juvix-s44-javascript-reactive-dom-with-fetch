/*
     FETCH METHOD
      - The fetch() method in javascript is used to request data from a server. The request can be of any type of API that returns the data in JSON or XML

      sample fetching :
        fetch ('https://jsonplacehonlder.typicode.com/post')

*/

fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((data) => console.log(data));

// add post data
// JOSN.stringify = object to string
// JSON.parse = string to object

// Making Post Request Using Fetch: Post requests can be made using fetch by giving options

fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((data) => showPosts(data));

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault();

  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: document.querySelector("#txt-title").value,
      body: document.querySelector("#txt-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully added");

      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-body").value = null;
    });
});

const showPosts = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `;
  });
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;

  // removeAttribute method - removes the 'disabled' attribute from the selected element// bale i erased nya ung text sa loob ng textbox then update sa console log
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// Update Post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault(); // preventDefault -
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT", // http method for update
    body: JSON.stringify({
      id: document.querySelector("#txt-edit-id").value,
      title: document.querySelector("#txt-edit-title").value,
      body: document.querySelector("#txt-edit-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Succesfully updated");
      document.querySelector("#txt-edit-id").value = null;
      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-body").value = null;

      // this code could be used to disable the submit button on a form
      // preventing the user from interacting the button and submitting the form

      // When a submit button is disabled, it appears grayed out and cannot be clicked
      document
        .querySelector("#btn-submit-update")
        .setAttribute("disabled", true); // bale kung rekta lng edit, nka disabled ung update button, dapat click ka muna ng isang edit button sa baba
    });
});

// === ACTIVITY =========
// const deletePost = (id) => {
//   fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
//     method: "DELETE",
//   }).then((data) => {
//     console.log(data);
//     alert("Successfully deleted");
//     document.querySelector(`#post-${id}`).remove();
//   });
// };

const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "DELETE",
  });
  document.querySelector(`#post-${id}`).remove();
};


